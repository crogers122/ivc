#ifndef IVCBACKEND__H
#define IVCBACKEND__H

#include <QMap>
#include <QObject>
#include <QLocalServer>
#include <QLocalSocket>
#include <QTimer>

#include <xen/be/Log.hpp>
#include <xen/be/XenStore.hpp>

#include "guestmanager.h"
#include "guestcontroller.h"
#include "libivc_core.h"
#include "ivcserver.h"

class IvcBackend : public QObject {
  Q_OBJECT
 public:
  IvcBackend(const QString &un_path);
  ~IvcBackend();

  GuestController* getController(domid_t domid);
  void removeProcess(IvcServer* srv);
  int sendControlMessage(const libivc_message_t& message);

 public slots:
  void onAddGuest(domid_t domid);
  void onRemoveGuest(domid_t domid);
  void addProcess();

 private:
  XenBackend::XenStore mXs;
  QLocalServer mProcessServer;
  GuestManager mManager;
  QMap<domid_t, GuestController*> mGuestControllers;
  QList<QLocalSocket*> mSockets;
  QList<IvcServer*> mServers;

  XenBackend::Log mLog;

  const QString mUnPath;
};

#endif //IVCBACKEND__H
