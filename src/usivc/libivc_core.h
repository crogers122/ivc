#ifndef LIBIVC_CORE__H
#define LIBIVC_CORE__H

#include <QMap>
#include <QDebug>

#include <thread>
#include <chrono>
#include <iostream>
#include <memory>
#include <mutex>
#include <system_error>
#include <functional>

#include <xen/be/Log.hpp>
#include <xen/be/XenStore.hpp>
#include <xen/be/XenGnttab.hpp>
#include <xen/be/XenEvtchn.hpp>

extern "C" {
#include <libivc.h>
#include <libivc_private.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
};

#include "ringbuf.h"
#include "ivc_client.h"

class libivc_core {
public:
    libivc_core();
    virtual ~libivc_core();

    void destroyClient(struct libivc_client *client);
    struct libivc_client *createClient(domid_t domid,
                                       uint16_t port,
                                       const grant_ref_t *grefs,
                                       uint32_t num_grants,
                                       evtchn_port_t evtport,
                                       uint64_t connection_id);
    void notifyRemote(struct libivc_client *client);
    int ivcRegisterCallbacks(struct libivc_client *client,
                             libivc_client_event_fired eventCallback,
                             libivc_client_disconnected disconnectCallback,
                             void *opaque);
    int ivcRecv(struct libivc_client *client, char *dest, size_t destSize);
    int ivcSend(struct libivc_client *client, char *dest, size_t destSize);
    int ivcAvailableData(struct libivc_client *client, size_t *dataSize);
    int ivcAvailableSpace(struct libivc_client *client, size_t *dataSize);

    int sendCtrlMsg(const struct libivc_client *client,
            MESSAGE_TYPE_T type, int16_t status);
    int handleConnectMessage(const libivc_message_t *msg);
    void handleDisconnectMessage(const libivc_message_t *msg);
    struct libivc_server *registerServer(uint16_t port,
                                         uint16_t domid,
                                         uint64_t client_id,
                                         libivc_client_connected cb,
                                         void *opaque);
    void shutdownServer(struct libivc_server *server);
    struct libivc_server *findServer(domid_t domid, uint16_t port);
private:
    void daemonDisconnect();
    int daemonConnect(const char *path);
    int daemonPoll();
    ssize_t daemonRecv(void *msg, size_t size);
    ssize_t daemonSend(const void *msg, size_t size);
    int daemonProcessMessage(const libivc_message_t *msg);
    void daemonMonitor();

    int sendCtrlMsg(uint16_t from_dom, uint16_t to_dom, uint16_t port,
            uint32_t event_channel, MESSAGE_TYPE_T type, uint32_t num_grants,
            uint64_t connection_id, int16_t status);

    uint32_t dom_port_key(uint16_t domid, uint16_t port);

    int mSock{-1};

    QMap<uint32_t, std::shared_ptr<ivcClient>> mClients;
    QMap<uint32_t, void *> mCallbackMap;
    QMap<uint32_t, void *> mCallbackArgumentMap;

    std::thread *mMonitor{nullptr};
    std::mutex mServerLock;
    std::mutex mClientLock;
    XenBackend::Log mLog;

    eventController mEventController;
};

static inline const char* messageTypeToString(unsigned int type) {
    static const char* strMessageType[] = {
        "MNONE",
        "CONNECT",
        "DISCONNECT",
        "ACK",
        "EVENT",
        "NOTIFY_ON_DEATH",
        "DOMAIN_DEAD",
        "MMAX"
    };

    if (type <= MNONE || type >= MMAX)
        return "INVALID";

    return strMessageType[type];
}

inline std::ostream& operator<<(std::ostream& os, const libivc_message_t& msg)
{
    return os << messageTypeToString(msg.type)
        << ": dom" << msg.from_dom << "->dom" << msg.to_dom
        << ":" << msg.port << ", " << msg.connection_id
        << ", evtchn:" << msg.event_channel
        << ", ngrefs:" << msg.num_grants
        << ", status: " << msg.status;
}

/*
 * Local variables:
 * mode: C++
 * c-file-style: "BSD"
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 */
#endif
