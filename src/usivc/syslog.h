#ifndef SYSLOG__H
#define SYSLOG__H

#include <iostream>
#include <string>

#include <syslog.h>

// See syslog(3)
namespace Syslog {
    enum struct Level {
        Emergency = LOG_EMERG,
        Alert = LOG_ALERT,
        Critical = LOG_CRIT,
        Error = LOG_ERR,
        Warning = LOG_WARNING,
        Notice = LOG_NOTICE,
        Info = LOG_INFO,
        Debug = LOG_DEBUG
    };

    class StreamBuf : public std::streambuf {
        public:
            // Just output at syslog's INFO level.
            StreamBuf(Level level = Level::Info) : mLevel(level) {};
        protected:
            int sync() {
                if (!mBuffer.empty())
                {
                    ::syslog(static_cast<int>(mLevel), "%s", mBuffer.c_str());
                    mBuffer.clear();
                }
                return 0;
            }
            int overflow(int c)
            {
                if (c == traits_type::eof())
                    sync();
                else
                    mBuffer += static_cast<char>(c);
                return c;
            }
        private:
            std::string mBuffer;
            enum Level mLevel;
    };
};

/* A single global StreamBuf for all XenBackend::Log instances */
extern Syslog::StreamBuf gSyslog;

#endif /* SYSLOG__H */
