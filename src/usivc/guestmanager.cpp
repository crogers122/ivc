#include "guestmanager.h"

GuestManager::GuestManager(XenBackend::XenStore &xs)
  : mXs(xs),
    mLog("GuestManager")
{
  mLog.setStreamBuffer(&gSyslog);
  // Watch @introduceDomain/@releaseDomain and check /local/domain to monitor
  // new guests
  mLocalDomainCallback = std::function<void(const std::string &)>([&](const std::string){ localDomainCallback(); });
  mXs.setWatch("@introduceDomain", mLocalDomainCallback);
  mXs.setWatch("@releaseDomain", mLocalDomainCallback);
  mXs.start();
}

GuestManager::~GuestManager()
{ }

bool GuestManager::containsDomain(const std::vector<domid_t> &domList, domid_t domid)
{
  for (const auto &d : domList) {
    if (domid == d) {
      return true;
    }
  }

  return false;
}

void GuestManager::syncRunningDomains(std::vector<domid_t> &runningDomains,
				      const std::vector<domid_t> &currentDomains)
{
  for(const auto &domid : currentDomains) {
    if(!containsDomain(runningDomains, domid)) {
      addDomain(runningDomains, domid);
    }
  }

  for(const auto &domid : runningDomains) {
    if(!containsDomain(currentDomains, domid)) {
      removeDomain(runningDomains, domid);
    }
  }
}

void GuestManager::addDomain(std::vector<domid_t> &domList, domid_t domid)
{
  if (!containsDomain(domList, domid)) {
    LOG(mLog, INFO) << "Add dom" << domid;
    domList.push_back(domid);
    emit addGuest(domid);
  }
}

void GuestManager::removeDomain(std::vector<domid_t> &domList, domid_t domid)
{
  int i = 0;
  for (const auto &d : domList) {
    if (domid == d) {
      LOG(mLog, INFO) << "Remove dom" << domid;
      domList.erase(domList.begin()+i);
      emit removeGuest(domid);
      return;
    }
    i++;
  }
}

void GuestManager::localDomainCallback()
{
    std::vector<std::string> directoryDomains = mXs.readDirectory("/local/domain");
    std::vector<domid_t> currentDomains;
    std::lock_guard<std::mutex> lock(mDomainListLock);

    for (const auto &d : directoryDomains) {
      domid_t domid = stoul(d);
      if (!containsDomain(currentDomains, domid)) {
        currentDomains.push_back(domid);
      }
    }

    if (currentDomains != mRunningDomains) {
      syncRunningDomains(mRunningDomains, currentDomains);
    }

    return;
}
