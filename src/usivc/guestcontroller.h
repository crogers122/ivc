#ifndef GUESTCONTROLLER__H
#define GUESTCONTROLLER__H

#include <iostream>
#include <memory>
#include <mutex>

#include <xen/be/Log.hpp>
#include <xen/be/XenStore.hpp>
#include <xen/be/XenGnttab.hpp>
#include <xen/be/XenEvtchn.hpp>

#include <QObject>

#include "ringbuf.h"

#include "libivc_core.h"
#include "syslog.h"

typedef enum BACKEND_STATUS {
    DISCONNECTED, CONNECTED, FAILED
} BACKEND_STATUS_T;

Q_DECLARE_METATYPE(libivc_message_t);

class GuestController : public QObject {
    Q_OBJECT
public:
    GuestController(XenBackend::XenStore &xs, domid_t domid);
    ~GuestController();

    int sendControlMessage(const libivc_message_t& msg);

signals:
    void clientMessage(const libivc_message_t msg);

private slots:
    void processControlEvent();

private:
    GuestController(const GuestController&) = delete;
    GuestController& operator=(const GuestController&) = delete;

    void initializeGuest(grant_ref_t gref, evtchn_port_t port);
    void finalizeGuest();
    void frontendCallback(const std::string &path);

    XenBackend::XenStore &mXs;
    domid_t mDomid;
    XenBackend::XenGnttabBuffer* mControlBuffer;
    ringbuf* mRb;
    XenBackend::XenEvtchn* mControlEvent;

    XenBackend::Log mLog;
};
/*
 * Local variables:
 * mode: C++
 * c-file-style: "BSD"
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 */
#endif //GUESTCONTROLLER__H
