#include "guestcontroller.h"
#include <xen/be/XenGnttab.hpp>
#include <QTimer>
#include <QFile>

GuestController::GuestController(XenBackend::XenStore &xs, domid_t domid)
    : mXs(xs),
      mDomid(domid),
      mControlBuffer(nullptr),
      mRb(nullptr),
      mControlEvent(nullptr),
      mLog("GuestController")
{
    mLog.setStreamBuffer(&gSyslog);
    qRegisterMetaType<libivc_message_t>();
    mXs.writeUint(mXs.getDomainPath(mDomid) + "/data/ivc/backend-status", DISCONNECTED);
    mXs.setWatch(mXs.getDomainPath(mDomid) + "/data/ivc",
            [=](const std::string path) { frontendCallback(path); });
}

GuestController::~GuestController()
{
    mXs.clearWatch(mXs.getDomainPath(mDomid) + "/data/ivc");
    finalizeGuest();
}

void GuestController::processControlEvent()
{
    libivc_message_t msg;
    int rc;

    if (mRb == nullptr) {
        LOG(mLog, WARNING) << "Failed to process control event: "
			<< "ring-buffer not initialized.";
        return;
    }

    do {
        rc = mRb->read_packet((uint8_t*)&msg, sizeof (msg));
        switch (rc) {
        case (sizeof (msg)):
            emit clientMessage(msg);
            /* fall through */
        case NO_DATA_AVAIL:
        case 0:
            break;
        default:
            LOG(mLog, WARNING) << "Failed to read control packet.";
        }
    } while (rc == sizeof (msg));
}

void
GuestController::initializeGuest(grant_ref_t gref, evtchn_port_t port)
{
    if (!gref) {
        LOG(mLog, WARNING) << "Failed to initialize control-channel for dom"
            << mDomid << ": " << "invalid grantref " << gref;
        return;
    }
    if (!port) {
        LOG(mLog, WARNING) << "Failed to initialize control-channel for dom"
            << mDomid << ": " << "invalid event-channel port" << port;
        return;
    }

    try {
        mControlBuffer = new XenBackend::XenGnttabBuffer(
                mDomid, gref, PROT_READ|PROT_WRITE);
    } catch (const std::exception& e) {
        mXs.writeUint(mXs.getDomainPath(mDomid) + "/data/ivc/backend-status",
                FAILED);
        mControlBuffer = nullptr;

        LOG(mLog, ERROR)
            << "Failed to create grant-table buffer for dom" << mDomid
            << ": " << e.what();
        return;
	}

    try {
        mRb = new ringbuf(
                reinterpret_cast<uint8_t*>(mControlBuffer->get()), 4096, true);
    } catch (const std::exception& e) {
        mXs.writeUint(mXs.getDomainPath(mDomid) + "/data/ivc/backend-status",
                FAILED);
        delete mControlBuffer;
        mControlBuffer = nullptr;

        LOG(mLog, ERROR)
            << "Failed to create IVC control ringbuffer with dom" << mDomid
            << ": " << e.what();
        return;
    }

    try {
        mControlEvent = new XenBackend::XenEvtchn(mDomid, port,
                [this]{ processControlEvent(); },
                [this](const std::exception& e) {
                    LOG(mLog, ERROR) << e.what();
                });
    } catch (const std::exception& e) {
        mXs.writeUint(mXs.getDomainPath(mDomid) + "/data/ivc/backend-status",
                FAILED);
        delete mRb;
        mRb = nullptr;
        delete mControlBuffer;
        mControlBuffer = nullptr;

        LOG(mLog, ERROR)
            << "Failed to bind event-channel for IVC control ringbuffer "
            << "with dom" << mDomid << ": " << e.what();
        return;
	}

	mControlEvent->start();

    mXs.writeUint(mXs.getDomainPath(mDomid) + "/data/ivc/backend-status",
            CONNECTED);

    LOG(mLog, INFO) << " IVC ctrl-channel initialized for dom" << mDomid;
}

void
GuestController::finalizeGuest()
{
    if (mControlEvent == nullptr)
        return;

    mControlEvent->stop();
    delete mControlEvent;

    if (mRb == nullptr)
        return;
    delete mRb;

    if (mControlBuffer == nullptr)
        return;
    delete mControlBuffer;
}


void
GuestController::frontendCallback(const std::string& path)
{
    unsigned int feState = 0, beState = 0;
    grant_ref_t gref = 0;
    evtchn_port_t port = 0;

    try {
        gref = mXs.readUint(path + "/frontend-page-rw");
        port = mXs.readUint(path + "/frontend-event");
        feState = mXs.readUint(path + "/frontend-status");
        beState = mXs.readUint(path + "/backend-status");
    } catch (...) {
        return;
    }

    //LOG(mLog, DEBUG) << " dom" << mDomid << ": gref:" << gref
    //    << " FE-state:" << feState << " BE-state:" << beState;

    switch (feState) {
        case INIT: // Wait for next change not an error, but FE is not READY.
            break;
        case READY:
            if (beState != CONNECTED)
                initializeGuest(gref, port);
            break;
        case DOWN:
            finalizeGuest();
            break;
        default:
            LOG(mLog, ERROR) << "Frontend reports invalid state: " << feState;
            break;
     }
}

int
GuestController::sendControlMessage(const libivc_message_t& msg)
{
    int rc;

    if (mRb == nullptr) {
        LOG(mLog, ERROR) << "Failed to send control message, ring is not "
            << "initialized.";
        return -ENODEV;
    }

    rc = mRb->write_packet(reinterpret_cast<const uint8_t*>(&msg),
            sizeof(msg));
    if (rc < 0) {
        LOG(mLog, ERROR) << "Failed to write control message on ring ("
            << rc << ")";
        return rc;
	}
    if (mRb->getEventEnabled())
        mControlEvent->notify();

    return rc;
}

/*
 * Local variables:
 * mode: C++
 * c-file-style: "BSD"
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 */
