#include <iostream>
#include "ivcbackend.h"

#include <QCoreApplication>

extern "C" {
#include <unistd.h>
}

int main(int argc, const char *argv[])
{
  QCoreApplication app(argc, (char **) argv);
  IvcBackend ivcbe("/var/run/ivc_control");

  app.exec();
  return 0;
}
